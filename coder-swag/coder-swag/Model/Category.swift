//
//  Category.swift
//  coder-swag
//
//  Created by Булат Хасанов on 12.11.2023.
//

import Foundation
class Category {
    public init (title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
    let title: String
    let imageName: String
}
