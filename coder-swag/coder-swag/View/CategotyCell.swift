//
//  CategotyCell.swift
//  coder-swag
//
//  Created by Булат Хасанов on 03.11.2023.
//

import UIKit

class CategotyCell: UITableViewCell {
    
    @IBOutlet weak var categotyImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func updateViews(categoty: Category) {
        categotyImage.image = UIImage(named: categoty.imageName)
        categoryTitle.text = categoty.title
    }

}
