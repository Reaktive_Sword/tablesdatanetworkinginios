//
//  ViewController.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 05.12.2023.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var movieTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieTable.dataSource = self
        movieTable.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getMovies().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell") as? MovieCell {
            let movie = DataService.instance.getMovies()[indexPath.row]
            cell.updateViews(movie: movie)
            
            return cell
        }
        
        return MovieCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = DataService.instance.getMovies()[indexPath.row]
        performSegue(withIdentifier: "ActorVC", sender: movie)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let actorVC = segue.destination as? ActorVC {
            
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            
            assert(sender as? Movie != nil)
            actorVC.initActors(movie: sender as! Movie)
            
        }
    }
}

