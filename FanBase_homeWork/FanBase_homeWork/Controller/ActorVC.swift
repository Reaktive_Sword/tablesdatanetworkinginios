//
//  ActorVC.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 13.12.2023.
//

import UIKit

class ActorVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var actorCollection: UICollectionView!
    
    private(set) public var actors = [Actor]()

    override func viewDidLoad() {
        super.viewDidLoad()

        actorCollection.dataSource = self
        actorCollection.delegate = self
    }
    
    func initActors(movie: Movie) {
        actors = DataService.instance.getActors()
        
        navigationItem.title = movie.name
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActorCell", for: indexPath) as? ActorCell {
            let actor = actors[indexPath.row]
            cell.updateView(actor: actor)
            return cell
        }
        
        return ActorCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let actor = DataService.instance.getActors()[indexPath.row]
        performSegue(withIdentifier: "LabelVC", sender: actor)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let labelVC = segue.destination as? LabelVC {
            
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            
            assert(sender as? Actor != nil)
            labelVC.initActorName(actor: sender as! Actor)
            
        }
    }
}
