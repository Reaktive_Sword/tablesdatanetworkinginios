//
//  LabelVC.swift
//  FanBase_homeWork
//
//  Created by Bulat Khasanov on 07/01/2024.
//

import UIKit

class LabelVC: UIViewController {

    @IBOutlet weak var actorName: UILabel!
    var name: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actorName.text = name
    }
    
    func initActorName(actor: Actor) {
        
        navigationItem.title = actor.name
        name = actor.name
    }

}
