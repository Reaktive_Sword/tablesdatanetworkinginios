//
//  MovieCell.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 06.12.2023.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieName: UILabel!

    func updateViews(movie: Movie) {
        movieName.text = movie.name
    }
}
