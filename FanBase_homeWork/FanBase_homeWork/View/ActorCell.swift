//
//  ActorCell.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 12.12.2023.
//

import UIKit

class ActorCell: UICollectionViewCell {
    @IBOutlet weak var actorName: UILabel!
    
    func updateView(actor: Actor) {
        actorName.text = actor.name
    }
}
