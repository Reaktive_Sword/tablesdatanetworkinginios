//
//  DataService.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 07.12.2023.
//

import Foundation
class DataService {
    static let instance = DataService()
    
    private let movies = [
        Movie(name: "Movie1"),
        Movie(name: "Movie2"),
        Movie(name: "Movie3"),
        Movie(name: "Movie4"),
        Movie(name: "Movie5"),
        Movie(name: "Movie6"),
        Movie(name: "Movie7")
    ]
    
    private let actors = [
        Actor(name: "Actor1"),
        Actor(name: "Actor2"),
        Actor(name: "Actor3"),
        Actor(name: "Actor4"),
        Actor(name: "Actor5")
    ]
    
    func getMovies() -> [Movie]{
        return self.movies
    }
    
    func getActors() -> [Actor] {
        return self.actors
    }
}
