//
//  Actor.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 12.12.2023.
//

import Foundation
struct Actor {
    private(set) public var name: String!
    
    init(name: String) {
        self.name = name
    }
}
