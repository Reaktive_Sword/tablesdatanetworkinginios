//
//  Movie.swift
//  FanBase_homeWork
//
//  Created by Булат Хасанов on 07.12.2023.
//

import Foundation

struct Movie {
    private(set) public var name: String
    
    init(name: String){
        self.name = name
    }
}
